# homepage-jk

This project represents my personal tinkering website currently available [here (in English)](https://en.johannes-kuenel.de/) or [here (in German)](https://johannes-kuenel.de/).
Everything is working in practice, but much work will have to be done for making it understandable and scalable. This will lead to changes so big that it would not make sense to write a full-scale description at this point.


# General concept and setup

The project uses the `flask` framework with some extensions for the backend and is served through `apache2` (which should not be a hard requirement and is only mentioned because this is the practical testing environment) with `mod_wsgi`.
In order to separate the sources from the actually served files and generally allow for processing between these two categories, the `src` directory contains the *source* files, which are *distributed* to the `dist` directory by `gulp`.
`npm` is used for managing `gulp` and its dependencies. In the current, debugging-friendly state, the only actual processing between `src` and `dist` is `scss` to `css` conversion.

The website is available in English and German, language handling is done by `babel`.
The resulting files have to be updated after altering text (TODO: add instructions. Skipping this for now because the official documentation is also good).
The other extensions for `flask` do not require any intervention and are automatically installed when using the provided `requirements.txt`.

The project relies on a virtual environment in `dist/env/` created by `virtualenv` (`venv` does not create `dist/env/bin/activate_this.py`. If you have to use it, you can copy this file from somewhere else).
This is useful for not polluting your machine with the required python packages.
Please make sure to use python 3 in the virtual environment and install all necessary packages (activate the environment by `. dist/env/bin/activate` and install them by `python3 -m pip install -r src/requirements.txt).
If you want to use the project without virtual environment, it should be possible by removing the activation part from `homepage.wsgi` and installing the required packages to your system.

Environment-specific settings are specified in `src/homepage/config.py`. In order to keep this file out of version control but still allow for an easy setup, `src/homepage/config.py.prototype` serves as a template that you can adjust and then copy or rename.
