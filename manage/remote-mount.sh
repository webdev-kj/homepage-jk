#! /usr/bin/env bash

source .config

login_user=$USER
dir_mount=$dir_root"remote/"

mkdir ${dir_mount} &> /dev/null

sshfs -o allow_root ${login_user}@${addr_remote}:${dir_remote_site} ${dir_mount} &

exit 0
