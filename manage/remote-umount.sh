#! /usr/bin/env bash

source .config

login_user=$USER
dir_mount=$dir_root"remote/"

umount ${dir_mount}

if [ ! "$(ls -A ${dir_mount})" ]; then
	rm -r ${dir_mount}
fi

exit 0
