#! /usr/bin/env bash

source .config

dir_dist=$dir_root"dist/"
dir_remote=$dir_root"remote/"
dir_manage="${dir_root}manage/"

did_mount=""
if [ ! -d ${dir_remote} ] || ( ! mountpoint "${dir_remote}" &> /dev/null ); then
	${dir_manage}remote-mount.sh
	did_mount="True"
fi

# Check if requirements for virtual environment have changed (easier to detect before sync)

requirements_changed=""
if ( ! diff ${dir_dist}/requirements.txt ${dir_remote}/requirements.txt > /dev/null); then
	requirements_changed="True"
fi

# Sync files

li_excepts=("__pycache__" "env" "tmp" "*.db")

str_excepts=""
for v in "${li_excepts[@]}"; do
	str_excepts+="--exclude='${v}' "
done

cmd_rsync="rsync -avc --delete ${str_excepts} '${dir_dist}' '${addr_remote}:${dir_remote_site}/'"
echo ${cmd_rsync}
eval ${cmd_rsync}

# Update remote virtual environment if requirements have changed (now new requirements list should be on remote)

if [[ ${requirements_changed} ]]; then
    echo "Requirements have changed. Will update remote virtual enivonment."
    su "${user_simple}" <<EOS
    ssh "${addr_remote}" "cd '${dir_remote_site}' && . env/bin/activate && pip install -r requirements.txt --upgrade && deactivate && echo 'Successfully updated virtual environment'"
EOS
fi

ssh "${addr_remote}" "systemctl restart apache2 && echo 'Successfully restarted apache'"

if [ ${did_mount} ]; then
	${dir_manage}remote-umount.sh
	did_mount=""
fi

exit 0
