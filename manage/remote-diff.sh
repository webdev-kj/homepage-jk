#! /usr/bin/env bash

source .config

dir_dist=$dir_root"dist/"
dir_remote=$dir_root"remote/"
dir_manage="${dir_root}manage/"

did_mount=""
if [ ! -d ${dir_remote} ]; then
	${dir_manage}remote-mount.sh
	did_mount="True"
fi

li_excepts=("__pycache__" "env" "tmp" "*.db")

str_excepts=""
for v in "${li_excepts[@]}"; do
	str_excepts+="-x \""${v}"\" "
done

cmd_diff="diff -bur ${str_excepts} ${dir_dist} ${dir_remote}"
#echo "Will execute: "${cmd_diff}
bash -c "${cmd_diff}"

if [ ${did_mount} ]; then
	${dir_manage}remote-umount.sh
	did_mount=""
fi

exit 0
