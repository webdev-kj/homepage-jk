#! /usr/bin/env bash

source .config

dir_dist=$dir_root"dist/"

file_hosts_tmp="hosts_tmp"

systemctl stop apache2
umount $dir_serv
umount $file_hosts_orig
rm $file_hosts_tmp

exit 0
