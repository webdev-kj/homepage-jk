#! /usr/bin/env bash

source .config

dir_manage="${dir_root}manage/"

"${dir_manage}local-start.sh"

su "${user_simple}" <<EOSU
cd $dir_root
gulp
EOSU

"${dir_manage}local-end.sh"

exit 0
