# Website management tools

This is a collection of tools for working on the website.


# Requirements

The tools assume that the website is hosted on a remote server and the development takes place on the local GNU/Linux machine.

Both machines should have `apache2` with mod-wsgi (`libapache2-mod-wsgi-py3`) installed, which allows a development version of the website to be served locally.
Generally, dependencies of the main project have to be installed and set up on the local machine as well.
Further dependency of the management tools that might not be installed on all systems are `sshfs`, `npm` and `gulp`.

Apache should be set up to serve the website on the url specified in `.config`. This usually begins with putting a config file in your apache2-sites-available directory, e.g. `/etc/apache2/sites-available`. This file should be similar to the one provided in [here](/supplements/local-example.de.conf). You can then enable the site (e.g. `a2ensite local-example.de`) and reload apache2 (`systemctl reload apache2`).

PLEASE NOTE:
- The scripts depend on a config file called `.config`. As this should not be checked into this repository, it has to be created following the scheme of `.config.prototype` (or, in practice, you can adjust and copy/rename `.config.prototype`).
- Depending on your system, the scripts may very well need root privileges.


# Usage

The standard workflow would consist of the following steps:
1. Mount the remote project directory on a local temp directory
2. Set up the local development server and start the local code processing
3. Do the development work (on the local project, not the mounted remote project)
4. Possibly check for changed files and database compability
5. Stop the local development server and code processing
6. Unmount the remote project directory

`session.sh` takes care of the setup and cleanup steps (1, 2, 5, 6).
If you are not planning to deploy, you do not have to mount and unmount the remote project directory.
In this case, use `local-session.sh`.
If you then realize you do need the remote project (e.g. for deployment), `remote-mount.sh` and `remote-umount.sh` will do what their names suggest.
`remote-diff.sh` shows differences between local and remote files.
`remote-checkdb.sh` checks whether the remote database (schema) needs to be updated.
`deploy.sh` deploys the local to the remote project by syncing the files and applying necessary operations (which, at the time of writing, includes installing missing dependencies of the virtual environment and restarting `apache2`).
Please note that remote database updates (e.g. adding a column) have to be done by hand as this seems to be difficult to automate.
