#! /usr/bin/env bash

source .config

dir_dist=$dir_root"dist/"

file_hosts_tmp="hosts_tmp"

cp -a $file_hosts_orig $file_hosts_tmp
echo "
127.0.0.1	${url_main}
127.0.0.1	${url_en}
" >> $file_hosts_tmp
mount --bind $file_hosts_tmp $file_hosts_orig
mount --bind $dir_dist $dir_serv
systemctl start apache2

exit 0
