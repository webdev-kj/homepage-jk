#! /usr/bin/env bash

source .config

dir_manage="${dir_root}manage/"

su "${user_simple}" <<EOSX
${dir_manage}remote-mount.sh
EOSX

${dir_manage}local-session.sh

su "${user_simple}" <<EOSY
${dir_manage}remote-umount.sh
EOSY

exit 0
