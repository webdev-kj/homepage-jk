#! /usr/bin/env bash

source .config

dir_dist=$dir_root"dist/"
dir_remote=$dir_root"remote/"
dir_manage="${dir_root}manage/"


did_mount=""
if [ ! -d ${dir_remote} ]; then
        ${dir_manage}remote-mount.sh
        did_mount="True"
fi

li_dbs=($(find ${dir_dist} -name "*.db"))

for db_dist in "${li_dbs[@]}"; do
	db_remote=${db_dist/${dir_dist}/${dir_remote}}
	db_base=${db_dist/${dir_dist}/""}
	#echo "Checking "${db_dist}
	if [ ! -f "${db_remote}" ]; then
		echo "==== Database ${db_base} not present in remote"
		continue
	fi
	li_tables_dist=($(sqlite3 ${db_dist} '.tables'))
	li_tables_remote=($(sqlite3 ${db_remote} '.tables'))
	for table_dist in "${li_tables_dist[@]}"; do
		if [[ ! " ${li_tables_remote[*]} " =~ " ${table_dist} " ]]; then
			echo "==== Table ${table_dist} of database ${db_base} not present in remote"
		fi
	done
	for table_remote in "${li_tables_remote[@]}"; do
		if [[ ! " ${li_tables_dist[*]} " =~ " ${table_remote} " ]]; then
			echo "=== Table ${table_remote} of database ${db_base} not present in dist"
			continue
		fi
		#echo "checking table ${table_remote}"
		schema_dist=$(sqlite3 ${db_dist} ".schema ${table_remote}")
		schema_remote=$(sqlite3 ${db_remote} ".schema ${table_remote}")
		if [ ! "${schema_dist}" == "${schema_remote}" ]; then
			echo "==== Differing schemas of table ${table_remote} in database ${db_base}:"
			echo "== Dist:"
			echo ${schema_dist}
			echo "== Remote:"
			echo ${schema_remote}
		fi
	done
done

if [ ${did_mount} ]; then
        ${dir_manage}remote-umount.sh
        did_mount=""
fi

exit 0
