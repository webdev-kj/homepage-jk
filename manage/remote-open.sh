#! /usr/bin/env bash

source .config

login_user=$USER
dir_mount=$dir_root"remote/"
dir_manage="${dir_root}manage/"

${dir_manage}remote-mount.sh

ssh ${login_user}@${addr_remote}

${dir_manage}remote-umount.sh

exit 0
