const {src, dest, watch, series, parallel} = require('gulp');
const sass = require('gulp-sass')(require('sass'));
//const uglify = require('gulp-uglify-es').default;

const files_src = {
    scss: 'src/homepage/static/scss/**/*.scss',
    js: 'src/homepage/static/js/**/*.js'
}

const paths_dist = {
    css: './dist/homepage/static/css',
    js: './dist/homepage/static/js'
}


function scssTask() {

    return src(files_src.scss)
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(dest(paths_dist.css));
}

function jsTask() {

    return src([files_src.js])
        //.pipe(uglify())
        .pipe(dest(paths_dist.js));
}

function remainderTask() {

    return src(['src/**/*', 'src/*', '!src/env/*', '!src/env/**/*', '!src/homepage/static/scss', '!src/homepage/static/scss/**/*.scss', '!src/homepage/static/js/**/*.js', '!src/**/*.pyc', '!src/homepage/storage/*', '!src/homepage/storage/**/*'])
        .pipe(dest('./dist'))
}

function watchTask() {

    watch([files_src.scss],
        parallel(scssTask));
    
    watch([files_src.js],
        parallel(jsTask));
    
    watch(['src/**'], {ignored: ['src/env/**', 'src/**/__pycache__/**', files_src.scss, files_src.js]},
        parallel(remainderTask));
}

exports.default = series(

    parallel(scssTask, jsTask),
    remainderTask,
    watchTask
)

exports.build = series(

    parallel(scssTask, jsTask),
    remainderTask
)