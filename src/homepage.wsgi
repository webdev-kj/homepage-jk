#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import pathlib

path_project = str(pathlib.Path(__file__).parent.absolute())

python_home = path_project + '/env'
activate_this = python_home + '/bin/activate_this.py'
exec(open(activate_this).read(), dict(__file__=activate_this))

sys.path.append(path_project)
sys.path.append(path_project + '/homepage')

from homepage import app as application
