#!/usr/bin/env python3

from homepage import app, db, path_app
from homepage.models import Captcha
import datetime
import os
import pathlib
from homepage import path_app


SECONDS_CAPTCHA_VALID = 10 * 60


with app.app_context():

    # Search for old captchas in database
    captchas_db = db.session.execute(db.select(Captcha)).scalars().fetchall()
    for captcha in captchas_db:
        if not captcha.created or (datetime.datetime.utcnow() > captcha.created and (datetime.datetime.utcnow() - captcha.created).seconds > SECONDS_CAPTCHA_VALID):
            db.session.delete(captcha)
            try:
                os.remove(path_app + '/static/' + captcha.filename)
            except Exception:
                pass
    db.session.commit()
    
    # Search for captcha images that are not present in database (should not exist, but just to be sure...)
    for captcha_img in pathlib.Path(path_app + '/static/tmp/captchas/').iterdir():
        filename = str(captcha_img.absolute()).split('/static/')[-1]
        if len(db.session.execute(db.select(Captcha).filter_by(filename = filename)).scalars().fetchall()) < 1:
            if captcha_img.is_file():
                captcha_img.unlink()
            else:
                # TODO: Error handling (e.g. log a warning) in case of finding a directory
                pass
