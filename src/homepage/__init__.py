# -*- coding: utf-8 -*-

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import pathlib
from flask_babel import Babel
from flask_cors import CORS

db = SQLAlchemy()

app = Flask(__name__)
CORS(app)

app.config.from_object("homepage.config")

db.init_app(app)

babel = Babel(app)

path_app = str(pathlib.Path(__file__).absolute().parent)
path_project = str(pathlib.Path(__file__).absolute().parent.parent)

from homepage import routes

if __name__ == "__main__":
	app.run(debug=False)
