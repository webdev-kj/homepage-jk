def random_str(length, charset = '123456789abcdefghijklmnpqrstuvwxyz'):

    import random

    res = ''

    for i in range(length):
        res += charset[random.randint(0, len(charset) - 1)]

    return res


def generate_captcha(length = 4):

    from captcha.image import ImageCaptcha
    from homepage import db
    from models import Captcha
    from homepage import path_app

    captcha_str = random_str(length)

    db_captcha = Captcha(value=captcha_str)
    db.session.add(db_captcha)
    db.session.commit()

    filename = 'tmp/captchas/' + str(db_captcha.id) + '.png'
    filedest = path_app + '/static/' + filename
    img_captcha = ImageCaptcha()
    img_captcha.write(captcha_str, filedest)

    db_captcha.filename = filename
    db.session.commit()

    return db_captcha