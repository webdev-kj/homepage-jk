# -*- coding: utf-8 -*-

from flask import render_template, url_for, request, redirect, send_from_directory, g
from homepage import app, forms, babel, path_app


def get_subdomain():
	return request.host[:-len(app.config['SERVER_NAME'])].rstrip('.')


@babel.localeselector
def get_locale():
	return 'en' if get_subdomain() == 'en' else 'de'


@app.route('/', subdomain='<sd>')
@app.route('/', subdomain='')
def home(sd=''):
	g.site = 'home'
	return render_template('home.jinja')

@app.route('/guitar-music-nuremberg/', subdomain='<sd>')
@app.route('/gitarre-nuernberg/', subdomain='')
def music(sd=''):
	g.site = 'music'
	return render_template('music.jinja')

@app.route('/guitar-music-nuremberg/free-sheet-music-download/', subdomain='<sd>')
@app.route('/gitarre-nuernberg/kostenlose-gitarrennoten-download/')
def sheetmusic(sd=''):
	g.site = 'sheetmusic'
	from homepage import data
	return render_template('sheetmusic.jinja', entries = (data.music_de if (sd == '') else data.music_en))

@app.route('/download/', methods=['GET', 'POST'], subdomain='<sd>')
@app.route('/download/', methods=['GET', 'POST'], subdomain='')
def download(sd=''):
	g.site = 'download'
	if request.method == 'GET' or not forms.FormCaptcha().validate_on_submit():
		id_sent = ('id' in request.args)
		form = None
		db_captcha = None
		img_captcha = None
		if (id_sent):
			from functions import generate_captcha
			db_captcha = generate_captcha()
			img_captcha = db_captcha.filename
			form = forms.FormCaptcha(captcha_id=str(db_captcha.id))
		return render_template('download.jinja', id_sent=id_sent, form=form, img_captcha = img_captcha)
	else:
		from werkzeug.exceptions import BadRequest, Unauthorized, UnprocessableEntity, FailedDependency, NotFound

		if 'id' not in request.args:
			return BadRequest('No download ID provided')
		from homepage import data
		for piece in (data.music_de if (sd == '') else data.music_en):
			if piece["id"] == request.args.get('id') and piece['sheetmusic']:
				return send_from_directory(directory=path_app+"/storage/", path=piece['sheetmusic'], mimetype='application/pdf', as_attachment=True)
		
		return NotFound('No download with this ID was found')

@app.route('/contact/', methods=['GET', 'POST'], subdomain='<sd>')
@app.route('/kontakt/', methods=['GET', 'POST'], subdomain='')
def contact(sd=''):
	g.site = 'contact'
	from functions import generate_captcha
	form = None
	success = None
	img_captcha = None
	if request.method == 'GET':
		db_captcha = generate_captcha()
		img_captcha = db_captcha.filename
		form = forms.FormContact(captcha_id=str(db_captcha.id))
		return render_template('contact.jinja', form=form, img_captcha=img_captcha, success=success)
	else:
		form = forms.FormContact()
		if form.validate_on_submit():
			from flask_mailman import Mail, EmailMessage
			mail = Mail()
			mail.init_app(app)
			msg = EmailMessage(
				subject='Inquiry through contact form',
				body=form.message.data,
				to=[app.config['MAIL_DEFAULT_RECEIVER']],
				bcc=[app.config['MAIL_DEFAULT_SENDER']],
				reply_to=[form.from_name.data + ' <' + form.from_email.data + '>']
				)
			try:
				msg.send(fail_silently=False)
				success = True
			except Exception as e:
				print('Error while sending email from ' + form.from_name.data + ' <' + form.from_email.data + '>')
				app.logger.error('Error while sending email from ' + form.from_name.data + ' <' + form.from_email.data + '>')
				app.logger.error(str(e))
				success = False
			return render_template('contact.jinja', form=None, img_captcha=img_captcha, success=success)
		else:
			db_captcha = generate_captcha()
			img_captcha = db_captcha.filename
			form.captcha_id.data = str(db_captcha.id)
			return render_template('contact.jinja', form=form, img_captcha=img_captcha, success=success)

@app.route('/site-notice/', subdomain='<sd>')
@app.route('/impressum/', subdomain='')
def sitenotice(sd=''):
	g.site = 'sitenotice'
	return render_template('sitenotice.jinja')

@app.route('/privacy-policy/', subdomain='<sd>')
@app.route('/datenschutz/', subdomain='')
def privacy(sd=''):
	g.site = 'privacy'
	return render_template('privacy.jinja')

@app.route('/projects/', subdomain='<sd>')
@app.route('/projekte/', subdomain='')
def projects(sd=''):
	g.site = 'projects'
	return render_template('projects.jinja')

@app.route('/projects/connect-four/', methods=['GET', 'POST'], subdomain='<sd>')
@app.route('/projekte/vier-gewinnt/', methods=['GET', 'POST'], subdomain='')
def connectfour(sd=''):
	g.site = 'connectfour'
	has_started = False
	form = None
	img_captcha = None
	game_data = None
	if request.method == 'GET':
		if request.args.get('session_id'):
			# Resume a game
			from models import SessionConnectfour
			sess = SessionConnectfour.query.get(request.args.get('session_id'))
			if sess == None:
				return redirect('')
			game_data = {
				"game_mode": sess.game_mode,
				"seconds_per_move": sess.seconds_per_move,
				"cols": sess.cols,
				"rows": sess.rows,
				"fields": sess.fields,
				"turn": sess.turn
			}
			has_started = True
		else:
			# Show blank form to start a new game
			from functions import generate_captcha
			db_captcha = generate_captcha()
			img_captcha = db_captcha.filename
			form = forms.FormStartConnectfour(captcha_id=str(db_captcha.id))
	else:
		form = forms.FormStartConnectfour()
		if form.validate_on_submit():
			# Valid request to start a new game
			from functions import random_str
			from homepage import db
			from models import SessionConnectfour
			import json

			session_id = random_str(8)
			new_fields = []
			for x in range(form.cols.data):
				new_fields.append([])
				for y in range(form.rows.data):
					new_fields[x].append(0)
					
			sess = SessionConnectfour(id=session_id, game_mode=int(form.game_mode.data), seconds_per_move=form.seconds_per_move.data, cols=form.cols.data, rows=form.rows.data, turn=1, fields=json.dumps(new_fields).replace(" ", ""))
			db.session.add(sess)
			db.session.commit()
			return redirect('?session_id=' + session_id)
		else:
			# Invalid request to start a new game
			from functions import generate_captcha
			db_captcha = generate_captcha()
			img_captcha = db_captcha.filename
			form.captcha_id.data = str(db_captcha.id)
	# Active game, or blank starting form, or starting form with new captcha
	return render_template('connectfour.jinja', js=['connectfour.js'], has_started=has_started, form=form, img_captcha=img_captcha, game_data=game_data)

@app.route('/projects/checkers/', methods=['GET', 'POST'], subdomain='<sd>')
@app.route('/projekte/dame/', methods=['GET', 'POST'], subdomain='')
def checkers(sd=''):
	g.site = 'checkers'
	has_started = False
	form = None
	img_captcha = None
	if request.method == 'GET':
		if request.args.get('session_id'):
			# Resume a game
			from models import SessionCheckers
			sess = SessionCheckers.query.get(request.args.get('session_id'))
			if sess == None:
				return redirect('')
			has_started = True
		else:
			# Show blank form to start a new game
			from functions import generate_captcha
			db_captcha = generate_captcha()
			img_captcha = db_captcha.filename
			form = forms.FormStartCheckers(captcha_id=str(db_captcha.id))
	else:
		form = forms.FormStartCheckers()
		if form.validate_on_submit():
			# Valid request to start a new game
			from functions import random_str
			from homepage import db, path_project
			from models import SessionCheckers
			import json
			import subprocess

			session_id = random_str(8)

			jar = path_project + "/apps/checkers_init.jar"
			command = "java -jar " + jar + " " + str(form.size.data)
			init_reply = subprocess.run(command.split(), capture_output=True, text=True).stdout.split("\n")[-2]
			if (init_reply == "-1"):
				from werkzeug.exceptions import FailedDependency
				return FailedDependency("Error while initializing board")
					
			sess = SessionCheckers(id=session_id, game_mode=int(form.game_mode.data), seconds_per_move=form.seconds_per_move.data, size=form.size.data, force_capture=form.force_capture.data, flying_queen=form.flying_queen.data, turn="w", result="o", fields=init_reply)
			db.session.add(sess)
			db.session.commit()
			return redirect('?session_id=' + session_id)
		else:
			# Invalid request to start a new game
			from functions import generate_captcha
			db_captcha = generate_captcha()
			img_captcha = db_captcha.filename
			form.captcha_id.data = str(db_captcha.id)
	# Active game, or blank starting form, or starting form with new captcha
	return render_template('checkers.jinja', js=['checkers.js'], has_started=has_started, form=form, img_captcha=img_captcha)

@app.route('/projects/chess/', methods=['GET', 'POST'], subdomain='<sd>')
@app.route('/projekte/schach/', methods=['GET', 'POST'], subdomain='')
def chess(sd=''):
	g.site = 'chess'
	has_started = False
	form = None
	img_captcha = None
	if request.method == 'GET':
		if request.args.get('session_id'):
			# Resume a game
			from models import SessionChess
			sess = SessionChess.query.get(request.args.get('session_id'))
			if sess == None:
				return redirect('')
			has_started = True
		else:
			# Show blank form to start a new game
			from functions import generate_captcha
			db_captcha = generate_captcha()
			img_captcha = db_captcha.filename
			form = forms.FormStartChess(captcha_id=str(db_captcha.id))
	else:
		form = forms.FormStartChess()
		if form.validate_on_submit():
			# Valid request to start a new game
			from functions import random_str
			from homepage import db, path_project
			from models import SessionChess
			import json
			import subprocess

			session_id = random_str(8)

			fen_start = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";	
			sess = SessionChess(id=session_id, game_mode=int(form.game_mode.data), seconds_per_move=form.seconds_per_move.data, fen=fen_start)
			db.session.add(sess)
			db.session.commit()
			return redirect('?session_id=' + session_id)
		else:
			# Invalid request to start a new game
			from functions import generate_captcha
			db_captcha = generate_captcha()
			img_captcha = db_captcha.filename
			form.captcha_id.data = str(db_captcha.id)
	# Active game, or blank starting form, or starting form with new captcha
	return render_template('chess.jinja', js=['chess.js'], has_started=has_started, form=form, img_captcha=img_captcha)


@app.route('/api/connectfour/getstate/', methods=['POST'])
def connectfour_getstate():

	from werkzeug.exceptions import BadRequest, Unauthorized, FailedDependency
	from flask import jsonify
	from models import SessionConnectfour

	request_data = request.get_json()
	if 'session_id' not in request_data:
		return BadRequest('session_id is mandatory')
	
	sent_id = request_data['session_id']
	
	sess = SessionConnectfour.query.get(sent_id)
	if sess == None:
		return Unauthorized('No session with this session_id was found')
	
	from games import games_connectfour_getstate

	try:
		response = games_connectfour_getstate(sess.fields, sess.turn, sess.game_mode)
	except Exception:
		return FailedDependency('Game logic could not process request. This is an internal error.')

	return jsonify(response)


@app.route('/api/connectfour/domove/', methods=['POST'])
def connectfour_domove():

	from werkzeug.exceptions import BadRequest, Unauthorized, FailedDependency
	from flask import jsonify
	import json
	from models import SessionConnectfour

	request_data = request.get_json()
	if 'session_id' not in request_data or 'move' not in request_data:
		return BadRequest('session_id and move are mandatory fields')
	
	sent_id = request_data['session_id']
	sent_move = request_data['move']
	
	sess = SessionConnectfour.query.get(sent_id)
	if sess == None:
		return Unauthorized('No session with this session_id was found')
	
	from games import games_connectfour_domove
	response = None
	try:
		response = games_connectfour_domove(sess.fields, sess.turn, sent_move)
	except Exception:
		return FailedDependency('Game logic could not process request. This is an internal error.')
	
	from homepage import db

	if response['success']:
		sess.turn = response['turn']
		sess.fields = json.dumps(response['board']).replace(" ", "")
		db.session.add(sess)
		db.session.commit()
	
	return jsonify(response)


@app.route('/api/connectfour/askagent/', methods=['POST'])
def connectfour_askagent():

	from werkzeug.exceptions import BadRequest, Unauthorized, FailedDependency
	from flask import jsonify
	import json
	from models import SessionConnectfour

	request_data = request.get_json()
	if 'session_id' not in request_data:
		return BadRequest('session_id is mandatory')
	
	sent_id = request_data['session_id']
	
	sess = SessionConnectfour.query.get(sent_id)
	if sess == None:
		return Unauthorized('No session with this session_id was found')
	
	if sess.game_mode == 3 or sess.game_mode == sess.turn:
		return BadRequest("It's not the computer's turn")
	
	confidence = sess.seconds_per_move * 1000
	
	from games import games_connectfour_getfrom_experiences

	move_db, experience_found = games_connectfour_getfrom_experiences(sess.fields, sess.turn, confidence)
	
	if move_db:
		# Move found in db of previously computed moves

		from games import games_connectfour_domove
		response = None
		try:
			response = games_connectfour_domove(sess.fields, sess.turn, move_db)
		except Exception:
			return FailedDependency('Game logic could not process request. This is an internal error.')

		from homepage import db

		if response['success']:
			sess.turn = response['turn']
			sess.fields = json.dumps(response['board']).replace(" ", "")
			db.session.add(sess)
			db.session.commit()
		else:
			return FailedDependency('Agent calculated invalid move. This is an internal error.')
		
		response['move'] = move_db
		
		return jsonify(response)
	
	# No move found in db of previously computed moves

	from games import games_connectfour_askagent

	response = None
	try:
		response = games_connectfour_askagent(sess.fields, sess.turn, sess.seconds_per_move)
	except Exception:
		return FailedDependency('Game logic could not process request. This is an internal error.')

	from games import games_connectfour_add_experience
	games_connectfour_add_experience(sess.fields, sess.turn, confidence, response['move'])

	from homepage import db
	sess.turn = response['turn']
	sess.fields = json.dumps(response['board']).replace(" ", "")
	db.session.add(sess)
	db.session.commit()
	
	return jsonify(response)


@app.route('/api/checkers/getstate/', methods=['POST'])
def checkers_getstate():

	from werkzeug.exceptions import BadRequest, Unauthorized, FailedDependency
	from flask import jsonify
	from models import SessionCheckers

	request_data = request.get_json()
	if 'session_id' not in request_data:
		return BadRequest('session_id is mandatory')
	
	sent_id = request_data['session_id']
	
	sess = SessionCheckers.query.get(sent_id)
	if sess == None:
		return Unauthorized('No session with this session_id was found')
	
	from games import games_checkers_getstate

	response = None
	try:
		response = games_checkers_getstate(sess.fields, sess.force_capture, sess.flying_queen, sess.turn, sess.game_mode)
	except Exception:
		return FailedDependency('Game logic could not process request. This is an internal error.')
	
	return jsonify(response)


@app.route('/api/checkers/domove/', methods=['POST'])
def checkers_domove():

	from werkzeug.exceptions import BadRequest, Unauthorized, FailedDependency
	from flask import jsonify
	import json
	from models import SessionCheckers

	request_data = request.get_json()
	if 'session_id' not in request_data or 'move' not in request_data:
		return BadRequest('session_id and move are mandatory')
	
	sent_id = request_data['session_id']
	sent_move = request_data['move']
	
	sess = SessionCheckers.query.get(sent_id)
	if sess == None:
		return Unauthorized('No session with this session_id was found')
	if (sess.game_mode == 1 and sess.turn == "b") or (sess.game_mode == 2 and sess.turn == "w"):
		return BadRequest("It's not the player's turn")

	from games import games_checkers_domove

	response = None
	try:
		response = games_checkers_domove(sess.fields, sess.force_capture, sess.flying_queen, sess.seconds_per_move, sess.game_mode, sess.turn, sent_move)
	except Exception:
		return FailedDependency('Game logic could not process request. This is an internal error.')
	
	from homepage import db
	sess.fields = json.dumps(response['board']).replace(" ", "")
	sess.result = response["endresult"]
	sess.turn = response["turn"]
	db.session.add(sess)
	db.session.commit()

	return jsonify(response)


@app.route('/api/checkers/askagent/', methods=['POST'])
def checkers_askagent():

	from werkzeug.exceptions import BadRequest, Unauthorized, FailedDependency
	from flask import jsonify
	import json
	from models import SessionCheckers

	request_data = request.get_json()
	if 'session_id' not in request_data:
		return BadRequest('session_id is mandatory')
	
	sent_id = request_data['session_id']
	
	sess = SessionCheckers.query.get(sent_id)
	if sess == None:
		return Unauthorized('No session with this session_id was found')
	
	turn = sess.turn
	if sess.game_mode == 3 or (sess.game_mode == 1 and turn == "w") or (sess.game_mode == 2 and turn == "b"):
		return BadRequest("It's not the computer's turn")
	
	confidence = sess.seconds_per_move * 1000

	from games import games_checkers_getfrom_experiences
	move_db, confidence_found = games_checkers_getfrom_experiences(sess.fields, sess.turn, confidence)

	if move_db:
		# Move found in db of previously computed moves

		from games import games_checkers_domove
		response = None
		try:
			response = games_checkers_domove(sess.fields, sess.force_capture, sess.flying_queen, sess.seconds_per_move, sess.game_mode, sess.turn, move_db)
		except Exception:
			return FailedDependency('Game logic could not process request. This is an internal error.')

		from homepage import db
		sess.fields = json.dumps(response['board']).replace(" ", "")
		sess.result = response["endresult"]
		sess.turn = response["turn"]
		db.session.add(sess)
		db.session.commit()

		response['move'] = move_db

		return jsonify(response)
	
	# No move found in db of previously computed moves

	from games import games_checkers_askagent

	response = None
	try:
		response = games_checkers_askagent(sess.fields, sess.force_capture, sess.flying_queen, sess.seconds_per_move, sess.game_mode, sess.turn)
	except Exception:
		return FailedDependency('Game logic could not process request. This is an internal error.')

	from games import games_checkers_add_experience
	games_checkers_add_experience(sess.fields, sess.turn, confidence, response['move'])

	from homepage import db
	sess.fields = json.dumps(response['board']).replace(" ", "")
	sess.result = response["endresult"]
	sess.turn = response["turn"]
	db.session.add(sess)
	db.session.commit()
	
	return jsonify(response)


@app.route('/api/chess/getstate/', methods=['POST'])
def chess_getstate():

	from werkzeug.exceptions import BadRequest, Unauthorized, FailedDependency
	from flask import jsonify
	from models import SessionChess

	request_data = request.get_json()
	if 'session_id' not in request_data:
		return BadRequest('session_id is mandatory')
	
	sent_id = request_data['session_id']
	
	sess = SessionChess.query.get(sent_id)
	if sess == None:
		return Unauthorized('No session with this session_id was found')

	from games import games_chess_getstate

	response = None
	try:
		response = games_chess_getstate(sess.fen, sess.game_mode, sess.seconds_per_move)
	except Exception:
		return FailedDependency('Game logic could not process request. This is an internal error.')
	
	return jsonify(response)


@app.route('/api/chess/domove/', methods=['POST'])
def chess_domove():

	from werkzeug.exceptions import BadRequest, Unauthorized, FailedDependency
	from flask import jsonify
	from models import SessionChess

	request_data = request.get_json()
	if 'session_id' not in request_data or 'move' not in request_data:
		return BadRequest('session_id and move are mandatory')
	
	sent_id = request_data['session_id']
	sent_move = request_data['move']
	
	sess = SessionChess.query.get(sent_id)
	if sess == None:
		return Unauthorized('No session with this session_id was found')
	
	turn = sess.fen.split(" ")[1]
	if (sess.game_mode == 1 and turn == "b") or (sess.game_mode == 2 and turn == "w"):
		return BadRequest("It's not the player's turn")
	
	from games import games_chess_domove
	response = None
	try:
		response = games_chess_domove(sess.fen, sess.game_mode, sess.seconds_per_move, sent_move)
	except Exception:
		return FailedDependency('Game logic could not process request. This is an internal error.')

	from homepage import db
	sess.fen = response['fen']
	sess.result = response['endresult']
	db.session.add(sess)
	db.session.commit()
	
	return jsonify(response)


@app.route('/api/chess/askagent/', methods=['POST'])
def chess_askagent():

	from werkzeug.exceptions import BadRequest, Unauthorized, FailedDependency
	from flask import jsonify
	from models import SessionChess

	request_data = request.get_json()
	if 'session_id' not in request_data:
		return BadRequest('session_id is mandatory')
	
	sent_id = request_data['session_id']
	
	sess = SessionChess.query.get(sent_id)
	if sess == None:
		return Unauthorized('No session with this session_id was found')
	
	turn = sess.fen.split(" ")[1]
	if sess.game_mode == 3 or (sess.game_mode == 1 and turn == "w") or (sess.game_mode == 2 and turn == "b"):
		return BadRequest("It's not the computer's turn")
	
	message = ""
	move_db = None

	from games import games_chess_getfrom_openings
	move_db, name_opening = games_chess_getfrom_openings(sess.fen)
	if move_db:
		# No move found in db of openings
		message = "From openings: " + name_opening

	confidence = sess.seconds_per_move * 1000

	if not move_db:
		# No move found in db of openings
		
		from games import games_chess_getfrom_experiences
		move_db, confidence_found = games_chess_getfrom_experiences(sess.fen, confidence)
		if move_db:
			# Move found in db of previously computed moves
			message = "From experiences with confidence " + str(confidence_found)
	
	if move_db:
		# Move found in db of openings or in db of previously computed moves
		
		from games import games_chess_domove
		response = None
		try:
			response = games_chess_domove(sess.fen, sess.game_mode, sess.seconds_per_move, move_db)
		except Exception:
			return FailedDependency('Game logic could not process request. This is an internal error.')

		from homepage import db
		sess.fen = response['fen']
		sess.result = response['endresult']
		db.session.add(sess)
		db.session.commit()

		response['move'] = move_db
		response['message'] = message
		
		return jsonify(response)
	
	# No move found in db of openings or in db of previously computed moves

	from games import games_chess_askagent
	response = None
	try:
		response = games_chess_askagent(sess.fen, sess.game_mode, sess.seconds_per_move)
	except Exception:
		return FailedDependency('Game logic could not process request. This is an internal error.')

	from games import games_chess_add_experience
	games_chess_add_experience(sess.fen, confidence, response['move'])

	from homepage import db
	sess.fen = response['fen']
	sess.result = response['endresult']
	db.session.add(sess)
	db.session.commit()
	
	return jsonify(response)
