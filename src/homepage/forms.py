# -*- coding: utf-8 -*-

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, HiddenField, RadioField, BooleanField, TextAreaField
from wtforms.fields import IntegerField
from wtforms.validators import ValidationError, InputRequired, NumberRange, AnyOf, Email
from flask_babel import lazy_gettext
from homepage import babel


def check_captcha(form, field):

    from models import Captcha

    db_captcha = Captcha.query.get(form.captcha_id.data)

    if db_captcha == None:
        raise ValidationError('Captcha database error')

    import os
    from homepage import path_app
    os.remove(path_app + '/static/' + db_captcha.filename)

    from homepage import db
    captcha_value = db_captcha.value
    db.session.delete(db_captcha)
    db.session.commit()
    
    if captcha_value != field.data:
        raise ValidationError('Captcha mismatch')


class FormStartConnectfour(FlaskForm):

    game_mode = RadioField(lazy_gettext('Modus'), choices=[('1', lazy_gettext('Ich beginne gegen Computer')), ('2', lazy_gettext('Computer beginnt')), ('3', lazy_gettext('Mensch vs Mensch'))], default='1', validators=[InputRequired(lazy_gettext('Pflichtfeld')), AnyOf(['1', '2', '3'], message=lazy_gettext('Ungültige Auswahl'))])
    seconds_per_move = IntegerField(lazy_gettext('Bedenkzeit für Computer'), default=10, validators=[InputRequired(lazy_gettext('Pflichtfeld')), NumberRange(min=3, max=20, message=lazy_gettext('Erlaubt:') +  '%(min)s - %(max)s')])
    cols = IntegerField(lazy_gettext('Spalten'), default=7, validators=[InputRequired(lazy_gettext('Pflichtfeld')), NumberRange(min=2, max=15, message=lazy_gettext('Erlaubt:') +  '%(min)s - %(max)s')])
    rows = IntegerField(lazy_gettext('Zeilen'), default=6, validators=[InputRequired(lazy_gettext('Pflichtfeld')), NumberRange(min=2, max=15, message=lazy_gettext('Erlaubt:') +  '%(min)s - %(max)s')])
    captcha_id = HiddenField('', validators=[InputRequired(lazy_gettext('Pflichtfeld'))])
    captcha = StringField(lazy_gettext('Captcha'), validators=[InputRequired(lazy_gettext('Pflichtfeld')), check_captcha])
    submit = SubmitField(lazy_gettext('Starten'))


class FormStartChess(FlaskForm):

    game_mode = RadioField(lazy_gettext('Modus'), choices=[('1', lazy_gettext('Ich beginne gegen Computer')), ('2', lazy_gettext('Computer beginnt')), ('3', lazy_gettext('Mensch vs Mensch'))], default='1', validators=[InputRequired(lazy_gettext('Pflichtfeld')), AnyOf(['1', '2', '3'], message='Ungültige Auswahl')])
    seconds_per_move = IntegerField(lazy_gettext('Bedenkzeit für Computer'), default=10, validators=[InputRequired(lazy_gettext('Pflichtfeld')), NumberRange(min=3, max=20, message=lazy_gettext('Erlaubt:') +  '%(min)s - %(max)s')])
    captcha_id = HiddenField('', validators=[InputRequired(lazy_gettext('Pflichtfeld'))])
    captcha = StringField(lazy_gettext('Captcha'), validators=[InputRequired(lazy_gettext('Pflichtfeld')), check_captcha])
    submit = SubmitField(lazy_gettext('Starten'))



class FormStartCheckers(FlaskForm):

    game_mode = RadioField(lazy_gettext('Modus'), choices=[('1', lazy_gettext('Ich beginne gegen Computer')), ('2', lazy_gettext('Computer beginnt')), ('3', lazy_gettext('Mensch vs Mensch'))], default='1', validators=[InputRequired(lazy_gettext('Pflichtfeld')), AnyOf(['1', '2', '3'], message='Ungültige Auswahl')])
    seconds_per_move = IntegerField(lazy_gettext('Bedenkzeit für Computer'), default=10, validators=[InputRequired(lazy_gettext('Pflichtfeld')), NumberRange(min=3, max=20, message=lazy_gettext('Erlaubt:') +  '%(min)s - %(max)s')])
    size = IntegerField(lazy_gettext('Brettgröße'), default=8, validators=[InputRequired(lazy_gettext('Pflichtfeld')), NumberRange(min=6, max=14, message=lazy_gettext('Erlaubt:') +  '%(min)s - %(max)s')])
    force_capture = BooleanField(lazy_gettext('Schlagzwang'))
    flying_queen = BooleanField(lazy_gettext('Damen ziehen beliebig weit'))
    captcha_id = HiddenField('', validators=[InputRequired(lazy_gettext('Pflichtfeld'))])
    captcha = StringField(lazy_gettext('Captcha'), validators=[InputRequired(lazy_gettext('Pflichtfeld')), check_captcha])
    submit = SubmitField(lazy_gettext('Starten'))


class FormCaptcha(FlaskForm):

    captcha_id = HiddenField('', validators=[InputRequired(lazy_gettext('Pflichtfeld'))])
    captcha = StringField(lazy_gettext('Captcha'), validators=[InputRequired(lazy_gettext('Pflichtfeld')), check_captcha])
    submit = SubmitField(lazy_gettext('Senden'))


class FormContact(FlaskForm):

    from_email = StringField(lazy_gettext('E-Mail'), validators=[InputRequired(lazy_gettext('Pflichtfeld')), Email()])
    from_name = StringField(lazy_gettext('Name'))
    message = TextAreaField(lazy_gettext('Nachricht'), validators=[InputRequired(lazy_gettext('Pflichtfeld'))])
    captcha_id = HiddenField('', validators=[InputRequired(lazy_gettext('Pflichtfeld'))])
    captcha = StringField(lazy_gettext('Captcha'), validators=[InputRequired(lazy_gettext('Pflichtfeld')), check_captcha])
    agree_tos = BooleanField(lazy_gettext('Datenschutzerklärung zustimmen'), validators=[InputRequired(lazy_gettext('Pflichtfeld'))])
    submit = SubmitField(lazy_gettext('Senden'))