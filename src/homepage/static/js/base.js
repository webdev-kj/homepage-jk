function cookiesAccept() {
    
    document.cookie = "cookieconsent=true; max-age=31536000; path=/";

    const cookieNotice = document.querySelector('.cookienotice');
    if (cookieNotice) cookieNotice.remove();
}