document.addEventListener("DOMContentLoaded", init);

function init() {
    
    console.log("Starting initializing connectfour")

    const boardContainer = document.getElementById("boardcontainer");
    
    if (!boardContainer) {
        console.error("Did not find boardcontainer");
        return false;
    }

    const board = new Connectfour(boardContainer, gameData.cols, gameData.rows, gameData.gameMode, gameData.secondsPerMove, gameData.fields, gameData.turn);

    board.drawBoard();

    if (hasStarted) {
        board.getState().then(response => {
            document.getElementById("afterboard").scrollIntoView({block: "end", behavior: "smooth"});
            board.startNextMove();
        })
    }
}

class Connectfour {

    constructor(container, cols, rows, playerMode, secondsPerMove, fields, turn) {
        
        this.guiContainer = container;

        this.cols = cols;
        this.rows = rows;
        this.secondsPerMove = secondsPerMove;
        this.guiFields = [];
        this.guiBoard = null;
        this.turn = turn;
        this.playerMode = playerMode;
        this.players = {};
        
        switch (playerMode) {
            case 1:
                this.players[1] = "human";
                this.players[2] = "computer";
                break;
            case 2:
                this.players[1] = "computer";
                this.players[2] = "human";
                break;
            case 3:
                this.players[1] = "human";
                this.players[2] = "human";
        }

        if (fields) {
            this.fields = fields;
            console.log("Set fields: ");
            console.log(this.fields);
        }
        else {
            this.fields = [];
            for (let col = 0; col < this.cols; col++) {
                this.fields[col] = new Array(this.rows);
                for (let row = 0; row < this.rows; row++) {
                    this.fields[col][row] = 0;
                }
            }
        }

        const url = new URL(window.location.href);
        this.session_id = url.searchParams.get("session_id");
    }

    startNextMove() {

        switch (this.endresult) {
            case 1:
            case 2:
                if (this.playerMode == 3) {
                    this.guiStatus.innerHTML = (this.endresult == 1 ? texts.resultPlayer1 : texts.resultPlayer2);
                }
                else {
                    if (this.players[this.endresult] == "computer") {
                        this.guiStatus.innerHTML = texts.resultLoose;
                    }
                    else {
                        this.guiStatus.innerHTML = texts.resultWin;
                    }
                }
                break;
            case 3:
                this.guiStatus.innerHTML = texts.resultTie;
                break;
            default:
                this.askForMove();
                break;
        }
    }

    askForMove() {

        if (this.players[this.turn] == "human") {
            this.askForMoveHuman();
        }
        else {
            this.askForMoveComputer();
        }
    }

    askForMoveComputer() {

        this.guiStatus.innerHTML = texts.turnComputer + '<span class="face-thinking"></span>';

        fetch(window.location.origin + '/api/connectfour/askagent/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                session_id: this.session_id
            })
        }).then(res => {
            if (res.ok) {
                return res.json();
            }
            else {
                alert("There has been a Problem: " + res.statusText)
            }
        }).then(data => {
            if (!data.success) {
                alert("There has been a Problem: " + data.message);
                return;
            }
            this.updateBoard(data.move);
            this.movesPossible = data.movesPossible;
            this.turn = data.turn;
            this.endresult = data.endresult;
            this.startNextMove();
        }).catch(error => {
            alert("Error during fetch, see console for details");
            console.log("Details for fetch error: ")
            console.log(error);
        })
    }

    askForMoveHuman() {

        if (this.playerMode == 3) {
            this.guiStatus.innerHTML = (this.turn == 1 ? texts.turnPlayer1 : texts.turnPlayer2);
        }
        else {
            this.guiStatus.innerHTML = texts.turnHuman;
        }

        for (let col of this.movesPossible) {

            this.guiFields[col][this.rows].setAttribute("data-move", String(this.turn));

            this.guiFields[col][this.rows].onclick = () => {
                for (let col of this.movesPossible) {
                    this.guiFields[col][this.rows].onclick = null;
                    this.guiFields[col][this.rows].setAttribute("data-move", "0");
                }
                this.doMoveHuman(col);
            }
        }
    }

    doMoveHuman(col) {

        fetch(window.location.origin + '/api/connectfour/domove/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                session_id: this.session_id,
                move: col
            })
        }).then(res => {
            if (res.ok) {
                return res.json();
            }
            else {
                alert("There has been a Problem: " + res.statusText)
            }
        }).then(data => {
            if (!data.success) {
                alert("There has been a Problem: " + data.message);
                return;
            }
            this.updateBoard(col);
            this.movesPossible = data.movesPossible;
            this.turn = data.turn;
            this.endresult = data.endresult;
            this.startNextMove();
        }).catch(error => {
            alert("Error during fetch, see console for details");
            console.log("Details for fetch error: ")
            console.log(error);
        })
    }

    updateBoard(move) {

        console.log("Updating board");
        for (let row = 0; row < this.rows; row++) {
            if (this.fields[move][row] <= 0) {
                this.fields[move][row] = this.turn;
                this.guiFields[move][row].coin.setAttribute("data-player", String(this.turn));
                break;
            }
        }
    }

    getState() {

        return new Promise((resolve, reject) => {
            fetch(window.location.origin + '/api/connectfour/getstate/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    session_id: this.session_id
                })
            }).then(res => {
                if (res.ok) {
                    return res.json();
                }
                else {
                    alert("There has been a Problem: " + res.statusText)
                    reject('Status not ok, but ' + res.statusText);
                }
            }).then(data => {
                this.movesPossible = data.movesPossible;
                this.fields = data.board;
                this.turn = data.turn;
                this.endresult = data.endresult;
                resolve('Got state sucessfully');
            }).catch(error => {
                alert("Error during fetch, see console for details");
                console.log("Details for fetch error: ")
                console.log(error);
                reject('Fetch error');
            })
        });
    }
    
    drawBoard() {

        this.guiFields = [];
        for (let col = 0; col < this.cols; col++) {
            this.guiFields[col] = new Array(this.rows + 1);
        }

        this.boardBoarder = this.guiContainer.appendChild(document.createElement("table"));
        this.boardBoarder.classList.add("board");

        const thead = this.boardBoarder.appendChild(document.createElement("thead"));
        const tr = thead.appendChild(document.createElement("tr"));
        this.guiStatus = tr.appendChild(document.createElement("th"));
        this.guiStatus.setAttribute("colspan", this.cols);
        this.guiStatus.classList.add("gamestatus");

        this.guiBoard = this.boardBoarder.appendChild(document.createElement("tbody"));

        for (let row = this.rows + 1; row >= 0; row--) {
            let rowElem = this.guiBoard.appendChild(document.createElement("tr"))
            for (let col = 0; col < this.cols; col++) {
                let field = rowElem.appendChild(document.createElement("td"));
                field.classList.add("field");
                field.setAttribute("data-row", row);
                field.setAttribute("data-col", col);
                field.coin = field.appendChild(document.createElement("div"));
                field.coin.classList.add("coin");
                if (row < this.rows) {
                    field.setAttribute("data-type", "regular");
                    field.coin.setAttribute("data-player", String(this.fields[col][row]));
                }
                else {
                    field.setAttribute("data-type", "entry");
                }
                
                this.guiFields[col][row] = field;
            }
        }

        this.resize();

        window.addEventListener("resize", () => {
            this.resize();
        });
    }

    resize() {

        const maxWidth = this.guiContainer.offsetWidth;
        const maxHeight = window.innerHeight * 0.8;

        const boardWidhtToHeight = this.cols / (this.rows + 2);

        let newWidth = 0;
        let newHeight = 0;

        if (boardWidhtToHeight > (maxWidth / maxHeight)) {
            newWidth = maxWidth;
            newHeight = newWidth / boardWidhtToHeight; 
        }
        else {
            newHeight = maxHeight;
            newWidth = newHeight * boardWidhtToHeight;
        }

        this.boardBoarder.width = newWidth;
        this.boardBoarder.height = newHeight;

        for (let row = 0; row < this.rows + 1; row++) {
            for (let col = 0; col < this.cols; col++) {
                this.guiFields[col][row].height = newHeight / (this.rows + 2);
            }
        }

        console.log("Finished resizing to (" + newWidth + "/" + newHeight + ")");
    }
}