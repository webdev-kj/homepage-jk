document.addEventListener("DOMContentLoaded", init);

function init() {
    
    console.log("Starting initializing chess")

    const boardContainer = document.getElementById("boardcontainer");
    
    if (!boardContainer) {
        console.error("Did not find boardcontainer");
        return false;
    }

    const board = new Chess(boardContainer);

    if (hasStarted) {
        board.getState().then(response => {
            board.drawBoard();
            board.setFields(board.fields);
            document.getElementById("afterboard").scrollIntoView({block: "end", behavior: "smooth"});
            board.startNextMove();
        });
    }
    else {
        board.size = 8;
        board.drawBoard();
    }
}

class Chess {

    constructor(container) {
        
        this.guiContainer = container;
        this.guiBoard = null;

        this.movesPossible = [];

        const url = new URL(window.location.href);
        this.session_id = url.searchParams.get("session_id");
    }

    startNextMove() {

        switch (this.endresult) {
            case "w":
            case "b":
                if (this.playerMode == 3) {
                    this.guiStatus.innerHTML = (this.endresult == "w" ? texts.resultPlayer1 : texts.resultPlayer2);
                }
                else {
                    if (this.players[this.endresult] == "computer") {
                        this.guiStatus.innerHTML = texts.resultLoose;
                    }
                    else {
                        this.guiStatus.innerHTML = texts.resultWin;
                    }
                }
                break;
            case "t":
                this.guiStatus.innerHTML = texts.resultTie;
                break;
            default:
                this.askForMove();
                break;
        }
    }

    askForMove() {

        if (this.players[this.turn] == "human") {
            this.askForMoveHuman();
        }
        else if (this.players[this.turn] == "computer") {
            this.askForMoveComputer();
        }
        else {
            throw "this.players[this.turn] neither 'human' nor 'computer' but " + this.players[this.turn] + " while this.turn is " + this.turn;
        }
    }

    askForMoveComputer() {

        this.guiStatus.innerHTML = texts.turnComputer + '<span class="face-thinking"></span>';

        fetch(window.location.origin + '/api/checkers/askagent/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                session_id: this.session_id
            })
        }).then(res => {
            if (res.ok) {
                return res.json();
            }
            else {
                alert("There has been a Problem: " + res.statusText)
            }
        }).then(data => {
            if (data.message) console.log("Agent says: " + data.message);
            this.setFields(data.board);
            this.setMoves(data.movesPossible);
            this.turn = data.turn;
            this.endresult = data.endresult;
            this.startNextMove();
        }).catch(error => {
            alert("Error during fetch, see console for details");
            console.log("Details for fetch error: ")
            console.log(error);
        })
    }

    askForMoveHuman() {

        if (this.playerMode == 3) {
            this.guiStatus.innerHTML = (this.turn == "w" ? texts.turnPlayer1 : texts.turnPlayer2);
        }
        else {
            this.guiStatus.innerHTML = texts.turnHuman;
        }

        for (let move of this.movesPossible) {
            this.guiFields[move.fromX][move.fromY].dataset.movable = "true";
        }
    }

    doMoveHuman(move) {

        fetch(window.location.origin + '/api/checkers/domove/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                session_id: this.session_id,
                move: move
            })
        }).then(res => {
            if (res.ok) {
                return res.json();
            }
            else {
                alert("There has been a Problem: " + res.statusText)
            }
        }).then(data => {
            this.setFields(data.board);
            this.setMoves(data.movesPossible);
            this.turn = data.turn;
            this.endresult = data.endresult;
            this.startNextMove();
        }).catch(error => {
            alert("Error during fetch, see console for details");
            console.log("Details for fetch error: ")
            console.log(error);
        })
    }

    getState() {

        return new Promise((resolve, reject) => {
            fetch(window.location.origin + '/api/checkers/getstate/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    session_id: this.session_id
                })
            }).then(res => {
                if (res.ok) {
                    return res.json();
                }
                else {
                    alert("There has been a Problem: " + res.statusText)
                    reject('Status not ok, but ' + res.statusText);
                }
            }).then(data => {
                this.endresult = data.endresult;
                this.fields = data.board;
                this.setMoves(data.movesPossible);
                this.secondsPerMove = data.secondsPerMove;
                this.size = data.board.length;

                this.turn = data.turn;
                this.playerMode = data.gamemode;
                this.players = {};
                
                switch (this.playerMode) {
                    case 1:
                        this.players.w = "human";
                        this.players.b = "computer";
                        break;
                    case 2:
                        this.players.w = "computer";
                        this.players.b = "human";
                        break;
                    case 3:
                        this.players.w = "human";
                        this.players.b = "human";
                }
                resolve('Got state sucessfully');
            }).catch(error => {
                alert("Error during fetch, see console for details");
                console.log("Details for fetch error: ")
                console.log(error);
                reject('Fetch error');
            })
        });
    }
    
    drawBoard() {

        this.guiFields = [];
        for (let col = 0; col < this.size; col++) {
            this.guiFields[col] = new Array(this.size);
        }

        this.boardBoarder = this.guiContainer.appendChild(document.createElement("table"));
        this.boardBoarder.classList.add("board");

        const thead = this.boardBoarder.appendChild(document.createElement("thead"));
        const tr = thead.appendChild(document.createElement("tr"));
        this.guiStatus = tr.appendChild(document.createElement("th"));
        this.guiStatus.setAttribute("colspan", this.size);
        this.guiStatus.classList.add("gamestatus");

        this.guiBoard = this.boardBoarder.appendChild(document.createElement("tbody"));

        for (let row = this.size - 1; row >= 0; row--) {
            let rowElem = this.guiBoard.appendChild(document.createElement("tr"))
            for (let col = 0; col < this.size; col++) {
                let field = rowElem.appendChild(document.createElement("td"));
                field.classList.add("field");
                field.setAttribute("data-fieldcolor", ((row + col) % 2 == 0) ? "black" : "white")
                field.setAttribute("data-row", row);
                field.setAttribute("data-col", col);
                this.guiFields[col][row] = field;

                field.onclick = () => {
                    if (field.dataset.movable == "true") {
                        document.querySelectorAll("[data-move]").forEach((fld) => {fld.removeAttribute("data-move")});
                        for (let move of this.movesPossible) {
                            if (move.fromX == col && move.fromY == row) {
                                this.guiFields[move.toX][move.toY].dataset.move = move.text;
                            }
                        }
                        return;
                    }
                    if (field.dataset.move) {
                        const move = field.dataset.move;
                        document.querySelectorAll("[data-move]").forEach((fld) => {fld.removeAttribute("data-move")});
                        document.querySelectorAll("[data-movable]").forEach((fld) => {fld.removeAttribute("data-movable")});
                        this.doMoveHuman(move);
                        return;
                    }
                    document.querySelectorAll("[data-move]").forEach((fld) => {fld.removeAttribute("data-move")});
                }
            }
        }

        this.resize();

        window.addEventListener("resize", () => {
            this.resize();
        });
    }

    setMoves(moves) {

        document.querySelectorAll("[data-movable]").forEach((fld) => {fld.removeAttribute("data-movable")});
        document.querySelectorAll("[data-move]").forEach((fld) => {fld.removeAttribute("data-move")});

        this.movesPossible = [];

        for (let mv_txt of moves) {
            
            let mv_obj = {text: mv_txt};

            mv_obj.stages = mv_txt.split("-");
            mv_obj.fromX = mv_obj.stages[0].charCodeAt(0) - 97;
            mv_obj.fromY = parseInt(mv_obj.stages[0].substring(1)) - 1;
            mv_obj.toX = mv_obj.stages[mv_obj.stages.length - 1].charCodeAt(0) - 97;
            mv_obj.toY = parseInt(mv_obj.stages[mv_obj.stages.length - 1].substring(1)) - 1;
            
            this.movesPossible.push(mv_obj);
        }
    }

    setFields(fields) {

        this.fields = fields;

        fields.forEach((column, indexCol) => {
            column.forEach((field, indexRow) => {
                let pieceCurrent = this.guiFields[indexCol][indexRow].querySelector(".piece");
                if ("wbWB".search(field) >= 0) {
                    const piecetype = (field == field.toLowerCase() ? "p" : "k");
                    const color = field.toLowerCase();
                    if (pieceCurrent) {
                        if (pieceCurrent.dataset.piecetype != piecetype || pieceCurrent.dataset.color != color) {
                            pieceCurrent.remove();
                            const pieceAdded = this.guiFields[indexCol][indexRow].appendChild(document.querySelector(`.pieceDeclaration [data-piecetype="${piecetype}"]`).cloneNode(true));
                            pieceAdded.dataset.color = color;
                        }
                    }
                    else {
                        const pieceAdded = this.guiFields[indexCol][indexRow].appendChild(document.querySelector(`.pieceDeclaration [data-piecetype="${piecetype}"]`).cloneNode(true));
                        pieceAdded.dataset.color = color;
                    }
                }
                else if (pieceCurrent) {
                    pieceCurrent.remove();
                }
            });
        });
    }

    resize() {

        const maxWidth = this.guiContainer.offsetWidth;
        const maxHeight = window.innerHeight * 0.8;

        const boardWidhtToHeight = 1;

        let newWidth = 0;
        let newHeight = 0;

        if (boardWidhtToHeight > (maxWidth / maxHeight)) {
            newWidth = maxWidth;
            newHeight = newWidth / boardWidhtToHeight; 
        }
        else {
            newHeight = maxHeight;
            newWidth = newHeight * boardWidhtToHeight;
        }

        this.boardBoarder.width = newWidth;
        this.boardBoarder.height = newHeight;
        
        for (let row = 0; row < this.size; row++) {
            for (let col = 0; col < this.size; col++) {
                this.guiFields[col][row].height = newHeight / (this.size);
                this.guiFields[col][row].width = newWidth / (this.size);
            }
        }

        console.log("Finished resizing to (" + newWidth + "/" + newHeight + ")");
    }
}