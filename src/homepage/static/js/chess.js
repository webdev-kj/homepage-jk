document.addEventListener("DOMContentLoaded", init);

function init() {
    
    console.log("Starting initializing chess")

    const boardContainer = document.getElementById("boardcontainer");
    
    if (!boardContainer) {
        console.error("Did not find boardcontainer");
        return false;
    }

    const board = new Chess(boardContainer);

    board.drawBoard();

    if (hasStarted) {
        document.getElementById("afterboard").scrollIntoView({block: "end", behavior: "smooth"});
        board.getState().then(response => {
            board.startNextMove();
        })
    }
}

class Chess {

    constructor(container) {
        
        this.guiContainer = container;
        this.guiBoard = null;

        const url = new URL(window.location.href);
        this.session_id = url.searchParams.get("session_id");
    }

    startNextMove() {

        switch (this.endresult) {
            case "w":
            case "b":
                if (this.playerMode == 3) {
                    this.guiStatus.innerHTML = (this.endresult == "w" ? texts.resultPlayer1 : texts.resultPlayer2);
                }
                else {
                    if (this.players[this.endresult] == "computer") {
                        this.guiStatus.innerHTML = texts.resultLoose;
                    }
                    else {
                        this.guiStatus.innerHTML = texts.resultWin;
                    }
                }
                break;
            case "t":
                this.guiStatus.innerHTML = texts.resultTie;
                break;
            default:
                this.askForMove();
                break;
        }
    }

    askForMove() {

        if (this.players[this.turn] == "human") {
            this.askForMoveHuman();
        }
        else {
            this.askForMoveComputer();
        }
    }

    askForMoveComputer() {

        this.guiStatus.innerHTML = texts.turnComputer + '<span class="face-thinking"></span>';

        fetch(window.location.origin + '/api/chess/askagent/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                session_id: this.session_id
            })
        }).then(res => {
            if (res.ok) {
                return res.json();
            }
            else {
                alert("There has been a Problem: " + res.statusText)
            }
        }).then(data => {
            if (data.message) console.log("Agent says: " + data.message);
            this.setFen(data.fen);
            this.setMoves(data.movesPossible);
            this.endresult = data.endresult;
            this.startNextMove();
        }).catch(error => {
            alert("Error during fetch, see console for details");
            console.log("Details for fetch error: ")
            console.log(error);
        })
    }

    askForMoveHuman() {

        if (this.playerMode == 3) {
            this.guiStatus.innerHTML = (this.turn == "w" ? texts.turnPlayer1 : texts.turnPlayer2);
        }
        else {
            this.guiStatus.innerHTML = texts.turnHuman;
        }

        for (let move of this.movesPossible) {
            this.guiFields[move.fromX][move.fromY].dataset.movable = "true";
        }
    }

    doMoveHuman(move) {

        fetch(window.location.origin + '/api/chess/domove/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                session_id: this.session_id,
                move: move
            })
        }).then(res => {
            if (res.ok) {
                return res.json();
            }
            else {
                alert("There has been a Problem: " + res.statusText)
            }
        }).then(data => {
            this.setFen(data.fen);
            this.setMoves(data.movesPossible);
            this.endresult = data.endresult;
            this.startNextMove();
        }).catch(error => {
            alert("Error during fetch, see console for details");
            console.log("Details for fetch error: ")
            console.log(error);
        })
    }

    getState() {

        return new Promise((resolve, reject) => {
            fetch(window.location.origin + '/api/chess/getstate/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    session_id: this.session_id
                })
            }).then(res => {
                if (res.ok) {
                    return res.json();
                }
                else {
                    alert("There has been a Problem: " + res.statusText)
                    reject('Status not ok, but ' + res.statusText);
                }
            }).then(data => {
                this.endresult = data.endresult;
                this.setFen(data.fen);
                this.setMoves(data.movesPossible);
                this.secondsPerMove = data.secondsPerMove;

                this.playerMode = data.gamemode;
                this.players = {};
                
                switch (this.playerMode) {
                    case 1:
                        this.players.w = "human";
                        this.players.b = "computer";
                        break;
                    case 2:
                        this.players.w = "computer";
                        this.players.b = "human";
                        break;
                    case 3:
                        this.players.w = "human";
                        this.players.b = "human";
                }
                resolve('Got state sucessfully');
            }).catch(error => {
                alert("Error during fetch, see console for details");
                console.log("Details for fetch error: ")
                console.log(error);
                reject('Fetch error');
            })
        });
    }
    
    drawBoard() {

        this.guiFields = [];
        for (let col = 0; col < 8; col++) {
            this.guiFields[col] = new Array(8);
        }

        this.boardBoarder = this.guiContainer.appendChild(document.createElement("table"));
        this.boardBoarder.classList.add("board");

        const thead = this.boardBoarder.appendChild(document.createElement("thead"));
        const tr = thead.appendChild(document.createElement("tr"));
        this.guiStatus = tr.appendChild(document.createElement("th"));
        this.guiStatus.setAttribute("colspan", 8);
        this.guiStatus.classList.add("gamestatus");

        this.guiBoard = this.boardBoarder.appendChild(document.createElement("tbody"));

        for (let row = 7; row >= 0; row--) {
            let rowElem = this.guiBoard.appendChild(document.createElement("tr"))
            for (let col = 0; col < 8; col++) {
                let field = rowElem.appendChild(document.createElement("td"));
                field.classList.add("field");
                field.setAttribute("data-fieldcolor", ((row + col) % 2 == 0) ? "black" : "white")
                field.setAttribute("data-row", row);
                field.setAttribute("data-col", col);
                this.guiFields[col][row] = field;

                field.onclick = () => {
                    if (field.dataset.movable == "true") {
                        document.querySelectorAll("[data-move]").forEach((fld) => {fld.removeAttribute("data-move")});
                        document.querySelectorAll(".promotionOverlay td").forEach((fld) => {fld.removeAttribute("data-promomove")});
                        document.querySelectorAll(".promotionOverlay").forEach((fld) => {fld.classList.remove("active")});
                        for (let move of this.movesPossible) {
                            if (move.fromX == col && move.fromY == row) {
                                this.guiFields[move.toX][move.toY].dataset.move = move.text;
                            }
                        }
                        return;
                    }
                    if (field.dataset.move) {
                        const move = field.dataset.move;
                        document.querySelectorAll("[data-move]").forEach((fld) => {fld.removeAttribute("data-move")});
                        if ("0O12345678".search(move[move.length - 1]) >= 0) {
                            document.querySelectorAll("[data-movable]").forEach((fld) => {fld.removeAttribute("data-movable")});
                            this.doMoveHuman(move);
                        }
                        else {
                            this.promotionOverlays[this.turn].classList.add("active");
                            for (let pieceType of ["q", "r", "b", "n"]) {
                                this.promotionOverlays[this.turn][pieceType].dataset.promomove = move.substring(0, move.length - 1) + (this.turn == "w" ? pieceType.toUpperCase() : pieceType);
                            }
                        }
                        return;
                    }
                    document.querySelectorAll(".promotionOverlay td").forEach((fld) => {fld.removeAttribute("data-promomove")});
                    document.querySelectorAll(".promotionOverlay").forEach((fld) => {fld.classList.remove("active")});
                    document.querySelectorAll("[data-move]").forEach((fld) => {fld.removeAttribute("data-move")});
                }
            }
        }


        this.promotionOverlays = [];

        for (let promotionColor of ["w", "b"]) {

            this.promotionOverlays[promotionColor] = document.createElement("div");
            this.promotionOverlays[promotionColor].classList.add("promotionOverlay", promotionColor);

            const promotionOverlayTable = this.promotionOverlays[promotionColor].appendChild(document.createElement("table"));
            const promotionOverlayTbody = promotionOverlayTable.appendChild(document.createElement("tbody"));
            const promotionOverlayRow = promotionOverlayTbody.appendChild(document.createElement("tr"));

            for (let pieceType of ["q", "r", "b", "n"]) {
                this.promotionOverlays[promotionColor][pieceType] = promotionOverlayRow.appendChild(document.createElement("td"));
                const pieceProm = this.promotionOverlays[promotionColor][pieceType].appendChild(document.querySelector(`.pieceDeclaration [data-piecetype="${pieceType}"]`).cloneNode(true));
                pieceProm.classList.add("pieceOverlay");
                pieceProm.dataset.color = promotionColor;
                this.promotionOverlays[promotionColor][pieceType].onclick = (e) => {
                    e.stopPropagation();
                    if (this.promotionOverlays[promotionColor][pieceType].dataset.promomove) {
                        const move = this.promotionOverlays[promotionColor][pieceType].dataset.promomove;
                        document.querySelectorAll(".promotionOverlay td").forEach((fld) => {fld.removeAttribute("data-promomove")});
                        document.querySelectorAll("[data-movable]").forEach((fld) => {fld.removeAttribute("data-movable")});
                        document.querySelectorAll("[data-move]").forEach((fld) => {fld.removeAttribute("data-move")});
                        this.promotionOverlays[this.turn].classList.remove("active");
                        this.doMoveHuman(move);
                    }
                }
            }
        }

        this.guiFields[2][7].appendChild(this.promotionOverlays.w);
        this.guiFields[2][0].appendChild(this.promotionOverlays.b);

        this.resize();

        window.addEventListener("resize", () => {
            this.resize();
        });
    }

    setMoves(moves) {


        document.querySelectorAll("[data-movable]").forEach((fld) => {fld.removeAttribute("data-movable")});
        document.querySelectorAll("[data-move]").forEach((fld) => {fld.removeAttribute("data-move")});

        this.movesPossible = [];

        for (let mv_txt of moves) {
            
            let mv_obj = {text: mv_txt};

            if (mv_txt == "0-0" || mv_txt == "0-0-0") {
                mv_obj.isCastling = true;
                mv_obj.fromX = 4;
                mv_obj.fromY = (this.turn == "w" ? 0 : 7);
                mv_obj.toX = (mv_txt == "0-0" ? 6 : 2);
                mv_obj.toY = mv_obj.fromY;
                mv_obj.pawnPromotion = false;
            }
            else {
                let txt_adjusted = mv_txt.replace("x", "");
                mv_obj.isCastling = false;
                mv_obj.fromX = txt_adjusted.charCodeAt(0) - 97;
                mv_obj.fromY = parseInt(txt_adjusted[1]) - 1;
                mv_obj.toX = txt_adjusted.charCodeAt(2) - 97;
                mv_obj.toY = parseInt(txt_adjusted[3]) - 1;
                mv_obj.pawnPromotion = (txt_adjusted.length > 4 ? txt_adjusted[4] : false);
            }

            this.movesPossible.push(mv_obj);
        }
    }

    setFen(fen) {

        this.fen = fen;

        let row = 7;
        let col = 0;

        const fenComponents = fen.split(" ");

        for (let char of fenComponents[0]) {

            if (char == "/") {
                row--;
                col = 0;
                continue;
            }

            let pieceCurrent = this.guiFields[col][row].querySelector(".piece:not(.pieceOverlay)");

            if (isNaN(char)) {
                const piecetype = char.toLowerCase();
                const color = (piecetype == char ? "b" : "w");
                if (pieceCurrent) {
                    if (pieceCurrent.dataset.piecetype != piecetype || pieceCurrent.dataset.color != color) {
                        pieceCurrent.remove();
                        const pieceAdded = this.guiFields[col][row].appendChild(document.querySelector(`.pieceDeclaration [data-piecetype="${piecetype}"]`).cloneNode(true));
                        pieceAdded.dataset.color = color;
                    }
                }
                else {
                    const pieceAdded = this.guiFields[col][row].appendChild(document.querySelector(`.pieceDeclaration [data-piecetype="${piecetype}"]`).cloneNode(true));
                    pieceAdded.dataset.color = color;
                }
                col++;
                continue;
            }

            let n = parseInt(char);

            while(n > 0) {
                let pieceCurrent = this.guiFields[col][row].querySelector(".piece:not(.pieceOverlay)");
                if (pieceCurrent) pieceCurrent.remove();
                col++;
                n--;
            }
        }

        this.turn = fenComponents[1];
    }

    resize() {

        const maxWidth = this.guiContainer.offsetWidth;
        const maxHeight = window.innerHeight * 0.8;

        const boardWidhtToHeight = 1;

        let newWidth = 0;
        let newHeight = 0;

        if (boardWidhtToHeight > (maxWidth / maxHeight)) {
            newWidth = maxWidth;
            newHeight = newWidth / boardWidhtToHeight; 
        }
        else {
            newHeight = maxHeight;
            newWidth = newHeight * boardWidhtToHeight;
        }

        this.boardBoarder.width = newWidth;
        this.boardBoarder.height = newHeight;
        
        for (let row = 0; row < 8; row++) {
            for (let col = 0; col < 8; col++) {
                this.guiFields[col][row].height = newHeight / (8 + 1);
            }
        }
    }
}