from flask import jsonify
import subprocess, json, sqlite3
from homepage import path_project


def games_connectfour_getstate(fields, turn, game_mode):

    response = {
        'success': True,
        'board': json.loads(fields),
        'turn': turn,
        'gamemode': game_mode,
        'movesPossible': [],
        'endresult': 0,
        'message': ''
    }

    jar = path_project + "/apps/connectfour_getstatus.jar"
    command = "java -jar " + jar + " " + fields
    agent_reply = subprocess.run(command.split(), capture_output=True, text=True).stdout.split("\n")[-2]

    agent_reply_components = agent_reply.split(" ")

    response['movesPossible'] = json.loads(agent_reply_components[0])
    response['endresultt'] = int(agent_reply_components[1])

    return response


def games_connectfour_domove(fields, turn, move):

    response = {
        'success': True,
        'board': [],
        'turn': 0,
        'movesPossible': [],
        'endresult': 0,
        'message': ''
    }

    jar = path_project + "/apps/connectfour_domove.jar"
    command = "java -jar " + jar + " " + str(move) + " " + fields + " " + str(turn)
    agent_reply = subprocess.run(command.split(), capture_output=True, text=True).stdout.split("\n")[-2]

    agent_reply_components = agent_reply.split(" ")
    if agent_reply_components[0] == "-4":
        response['success'] = False
        response['message'] = 'Board rejected the move'
        return response

    response['board'] = json.loads(agent_reply_components[0])
    response['movesPossible'] = json.loads(agent_reply_components[1])
    response['endresult'] = int(agent_reply_components[2])
    response['turn'] = int(agent_reply_components[3])

    return response


def games_connectfour_askagent(fields, turn, seconds):

    response = {
		'success': True,
		'move': -1,
		'board': [],
		'turn': 0,
		'movesPossible': [],
		'endresult': 0,
		'message': ''
	}

    jar = path_project + "/apps/connectfour_askagent.jar"
    command = "java -jar " + jar + " " + fields + " " + str(turn) + " " + str(seconds * 1000)
    agent_reply = subprocess.run(command.split(), capture_output=True, text=True).stdout.split("\n")[-2]

    agent_reply_components = agent_reply.split(" ")

    response['move'] = int(agent_reply_components[0])
    response['board'] = json.loads(agent_reply_components[1])
    response['movesPossible'] = json.loads(agent_reply_components[2])
    response['endresult'] = int(agent_reply_components[3])
    response['turn'] = int(agent_reply_components[4])
    response['success'] = (response['move'] >= 0)

    return response


def games_checkers_getstate(fields, force_capture, flying_queen, turn, game_mode):
	
    response = {
        'success': True,
        'board': json.loads(fields),
        'turn': turn,
        'gamemode': game_mode,
        'movesPossible': [],
        'endresult': [],
        'message': ''
    }

    jar = path_project + "/apps/checkers_status.jar"
    command = ["java", "-jar", jar, fields, turn, ("1" if force_capture else "0"), ("1" if flying_queen else "0")]
    agent_reply = subprocess.run(command, capture_output=True, text=True).stdout.split("\n")

    agent_reply_components = {}

    wanted_labels = ["moves", "endresult"]
    num_wanted_labels = len(wanted_labels)
    for line in agent_reply:
        if line.startswith("error"):
            raise Exception('CLI output included error:\n' + str(line))
        line_components = line.split(": ")
        if line_components[0] in wanted_labels:
            agent_reply_components[line_components[0]] = line_components[1]
            if len(agent_reply_components) == num_wanted_labels:
                break

    if len(agent_reply_components) != num_wanted_labels:
        raise Exception('Unexpected number of CLI output lines')

    agent_reply_components["moves"] = json.loads(agent_reply_components["moves"])

    response['movesPossible'] = agent_reply_components["moves"]
    response['endresult'] = agent_reply_components["endresult"]

    return response


def games_checkers_domove(fields, force_capture, flying_queen, seconds_per_move, game_mode, turn, move):

    response = {
        'board': None,
        'movesPossible': [],
        'endresult': None,
        'turn': None,
        'gamemode': game_mode,
        'seccondsPerMove': seconds_per_move,
        'message': ''
    }

    jar = path_project + "/apps/checkers_domove.jar"
    command = ['java', '-jar', jar, fields, turn, ("1" if force_capture else "0"), ("1" if flying_queen else "0"), move]
    agent_reply = subprocess.run(command, capture_output=True, text=True).stdout.split("\n")

    agent_reply_components = {}

    wanted_labels = ["board", "moves", "endresult"]
    num_wanted_labels = len(wanted_labels)
    for line in agent_reply:
        if line.startswith("error"):
            raise Exception('CLI output included error:\n' + str(line))
        line_components = line.split(": ")
        if line_components[0] in wanted_labels:
            agent_reply_components[line_components[0]] = line_components[1]
            if len(agent_reply_components) == num_wanted_labels:
                break

    if len(agent_reply_components) != num_wanted_labels:
        raise Exception('Unexpected number of CLI output lines')
    
    response['board'] = json.loads(agent_reply_components['board'])
    response['movesPossible'] = json.loads(agent_reply_components['moves'])
    response['endresult'] = agent_reply_components['endresult']
    response['turn'] = ("w" if turn == "b" else "b")

    return response


def games_checkers_askagent(fields, force_capture, flying_queen, seconds_per_move, game_mode, turn):

    response = {
        'move': None,
        'board': None,
        'movesPossible': [],
        'endresult': None,
        'turn': None,
        'gamemode': game_mode,
        'seccondsPerMove': seconds_per_move,
        'message': ''
    }

    jar = path_project + "/apps/checkers_askagent.jar"
    command = ['java', '-jar', jar, fields, turn, ("1" if force_capture else "0"), ("1" if flying_queen else "0"), str(seconds_per_move * 1000)]
    agent_reply = subprocess.run(command, capture_output=True, text=True).stdout.split("\n")

    agent_reply_components = {}

    wanted_labels = ["board", "move", "moves", "endresult"]
    num_wanted_labels = len(wanted_labels)
    for line in agent_reply:
        if line.startswith("error"):
            raise Exception('CLI output included error:\n' + str(line))
        line_components = line.split(": ")
        if line_components[0] in wanted_labels:
            agent_reply_components[line_components[0]] = line_components[1]
            if len(agent_reply_components) == num_wanted_labels:
                break

    if len(agent_reply_components) != num_wanted_labels:
        raise Exception('Unexpected number of CLI output lines')
    
    response['board'] = json.loads(agent_reply_components['board'])
    response['move'] = agent_reply_components['move']
    response['movesPossible'] = json.loads(agent_reply_components['moves'])
    response['endresult'] = agent_reply_components['endresult']
    response['turn'] = ("w" if turn == "b" else "b")

    return response


def games_chess_getstate(fen, game_mode, seconds_per_move):

    response = {
        'fen': fen,
        'gamemode': game_mode,
        'movesPossible': [],
        'endresult': [],
        'seccondsPerMove': seconds_per_move,
        'message': ''
    }

    jar = path_project + "/apps/chess_status.jar"
    command = ['java', '-jar', jar, fen]
    agent_reply = subprocess.run(command, capture_output=True, text=True).stdout.split("\n")

    agent_reply_components = {}

    wanted_labels = ["moves", "endresult"]
    num_wanted_labels = len(wanted_labels)
    for line in agent_reply:
        if line.startswith("error"):
            raise Exception('CLI output included error:\n' + str(line))
        line_components = line.split(": ")
        if line_components[0] in wanted_labels:
            agent_reply_components[line_components[0]] = line_components[1]
            if len(agent_reply_components) == num_wanted_labels:
                break

    if len(agent_reply_components) != num_wanted_labels:
        raise Exception('Unexpected number of CLI output lines')

    response['movesPossible'] = json.loads(agent_reply_components["moves"])
    response['endresult'] = agent_reply_components['endresult']

    return response


def games_chess_domove(fen, game_mode, seconds_per_move, move):

    response = {
        'fen': '',
        'gamemode': game_mode,
        'movesPossible': [],
        'endresult': [],
        'seccondsPerMove': seconds_per_move,
        'message': ''
    }

    jar = path_project + "/apps/chess_domove.jar"
    command = ['java', '-jar', jar, fen, move]
    agent_reply = subprocess.run(command, capture_output=True, text=True).stdout.split("\n")

    agent_reply_components = {}

    wanted_labels = ["fen", "moves", "endresult"]
    num_wanted_labels = len(wanted_labels)
    for line in agent_reply:
        if line.startswith("error"):
            raise Exception('CLI output included error:\n' + str(line))
        line_components = line.split(": ")
        if line_components[0] in wanted_labels:
            agent_reply_components[line_components[0]] = line_components[1]
            if len(agent_reply_components) == num_wanted_labels:
                break

    if len(agent_reply_components) != num_wanted_labels:
        raise Exception('Unexpected number of CLI output lines')

    response['movesPossible'] = json.loads(agent_reply_components['moves'])
    response['endresult'] = agent_reply_components['endresult']
    response['fen'] = agent_reply_components['fen']

    return response
	

def games_chess_askagent(fen, game_mode, seconds_per_move):

    response = {
        'fen': '',
        'move': '',
        'gamemode': game_mode,
        'movesPossible': [],
        'endresult': [],
        'seccondsPerMove': seconds_per_move,
        'message': ''
    }

    jar = path_project + "/apps/chess_askagent.jar"
    command = ['java', '-jar', jar, fen, str(seconds_per_move * 1000)]
    agent_reply = subprocess.run(command, capture_output=True, text=True).stdout.split("\n")

    agent_reply_components = {}

    wanted_labels = ["fen", "move", "moves", "endresult"]
    num_wanted_labels = len(wanted_labels)
    for line in agent_reply:
        if line.startswith("error"):
            raise Exception('CLI output included error:\n' + str(line))
        line_components = line.split(": ")
        if line_components[0] in wanted_labels:
            agent_reply_components[line_components[0]] = line_components[1]
            if len(agent_reply_components) == num_wanted_labels:
                break

    if len(agent_reply_components) != num_wanted_labels:
        raise Exception('Unexpected number of CLI output lines')

    response['movesPossible'] = json.loads(agent_reply_components["moves"])
    response['endresult'] = agent_reply_components['endresult']
    response['fen'] = agent_reply_components['fen']
    response['move'] = agent_reply_components['move']

    return response


def games_connectfour_getfrom_experiences(fields, turn, confidence):

    gamestate = fields + str(turn)

    conn = sqlite3.connect(path_project + "/apps/connectfour_experiences.db")
    cursor = conn.cursor()
    cursor.execute("SELECT move, confidence FROM experiences WHERE gamestate=? AND confidence>=?", (gamestate, confidence))
    move_db = cursor.fetchone()
    cursor.close()
    conn.close()

    if move_db:
        move = move_db[0]
        confidence_found = move_db[1]
        return move, confidence_found

    return None, None


def games_connectfour_add_experience(fields, turn, confidence, move):

    gamestate = fields + str(turn)

    conn = sqlite3.connect(path_project + "/apps/connectfour_experiences.db")
    cursor = conn.cursor()
    cursor.execute("INSERT into experiences(gamestate, move, confidence) VALUES(?, ?, ?) ON CONFLICT(gamestate) DO UPDATE SET move=excluded.move, confidence=excluded.confidence;", (gamestate, move, confidence))
    conn.commit()
    cursor.close()
    conn.close()


def games_checkers_getfrom_experiences(fields, turn, confidence):

    gamestate = fields + turn

    conn = sqlite3.connect(path_project + "/apps/checkers_experiences.db")
    cursor = conn.cursor()
    cursor.execute("SELECT move, confidence FROM experiences WHERE gamestate=? AND confidence>=?", (gamestate, confidence))
    move_db = cursor.fetchone()
    cursor.close()
    conn.close()

    if move_db:
        move = move_db[0]
        confidence_found = move_db[1]
        return move, confidence_found

    return None, None


def games_checkers_add_experience(fields, turn, confidence, move):

    gamestate = fields + turn

    conn = sqlite3.connect(path_project + "/apps/checkers_experiences.db")
    cursor = conn.cursor()
    cursor.execute("INSERT into experiences(gamestate, move, confidence) VALUES(?, ?, ?) ON CONFLICT(gamestate) DO UPDATE SET move=excluded.move, confidence=excluded.confidence;", (gamestate, move, confidence))
    conn.commit()
    cursor.close()
    conn.close()


def games_chess_getfrom_openings(fen):

    fen_reduced = " ".join(fen.split(" ")[:4])

    conn = sqlite3.connect(path_project + "/apps/chess_openings.db")
    cursor = conn.cursor()
    cursor.execute("SELECT move, name FROM openings WHERE fen_reduced=?", (fen_reduced, ))
    move_db = cursor.fetchone()
    cursor.close()
    conn.close()

    if move_db:
        move = move_db[0]
        name = move_db[1]
        return move, name
    
    return None, None


def games_chess_getfrom_experiences(fen, confidence):

    fen_reduced = " ".join(fen.split(" ")[:4])

    conn = sqlite3.connect(path_project + "/apps/chess_experiences.db")
    cursor = conn.cursor()
    cursor.execute("SELECT move, confidence FROM experiences WHERE fen_reduced=? AND confidence>=?", (fen_reduced, confidence))
    move_db = cursor.fetchone()
    cursor.close()
    conn.close()

    if move_db:
        move = move_db[0]
        confidence_found = move_db[1]
        return move, confidence_found
    
    return None, None


def games_chess_add_experience(fen, confidence, move):

    fen_reduced = " ".join(fen.split(" ")[:4])

    conn = sqlite3.connect(path_project + "/apps/chess_experiences.db")
    cursor = conn.cursor()
    cursor.execute("INSERT into experiences(fen_reduced, move, confidence) VALUES(?, ?, ?) ON CONFLICT(fen_reduced) DO UPDATE SET move=excluded.move, confidence=excluded.confidence;", (fen_reduced, move, confidence))
    conn.commit()
    cursor.close()
    conn.close()