# -*- coding: utf-8 -*-

from homepage import db
import datetime


class Captcha(db.Model):

	id = db.Column(db.Integer, primary_key=True)
	value = db.Column(db.String(8), nullable=False)
	filename = db.Column(db.String(64))
	created = db.Column(db.DateTime, default=datetime.datetime.utcnow)


class SessionConnectfour(db.Model):

	id = db.Column(db.String(16), primary_key=True)
	game_mode = db.Column(db.Integer, nullable=False)
	seconds_per_move = db.Column(db.Integer, nullable=False)
	cols = db.Column(db.Integer, nullable=False)
	rows = db.Column(db.Integer, nullable=False)
	fields = db.Column(db.Text)
	turn = db.Column(db.Integer, nullable=False)
	result = db.Column(db.Integer)


class SessionCheckers(db.Model):

	id = db.Column(db.String(16), primary_key=True)
	game_mode = db.Column(db.Integer, nullable=False)
	seconds_per_move = db.Column(db.Integer, nullable=False)
	size = db.Column(db.Integer, nullable=False)
	fields = db.Column(db.Text)
	turn = db.Column(db.Text, nullable=False)
	force_capture = db.Column(db.Boolean, nullable=False)
	flying_queen = db.Column(db.Boolean, nullable=False)
	result = db.Column(db.Text)


class SessionChess(db.Model):

	id = db.Column(db.String(16), primary_key=True)
	game_mode = db.Column(db.Integer, nullable=False)
	seconds_per_move = db.Column(db.Integer, nullable=False)
	fen = db.Column(db.Text)
	result = db.Column(db.Text)